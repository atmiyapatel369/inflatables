const gulp = require('gulp');
var uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const del = require('del');
var pipeline = require('readable-stream').pipeline;
var cleanCSS = require('gulp-clean-css');

var sourcemaps = require('gulp-sourcemaps');

gulp.task('styles', () => {
    return gulp.src('./style/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./style/css/'))
});

gulp.task('clean', () => {
    return del([
        'css/main.css',
    ]);
});

gulp.task('mini-css', () => {
    return gulp.src('./style/css/main.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./style/css/dist/'));
});


gulp.task('compress', function () {
    return pipeline(
          gulp.src('./style/css/main.css'),
          uglify(),
          gulp.dest('./style/css/dist/')
    );
  });

gulp.task('watch', () => {
    gulp.watch('./style/sass/**/*.scss', (done) => {
        gulp.series(['clean', 'styles','mini-css'])(done);
    });
});

gulp.task('default', gulp.series(['clean', 'styles','mini-css']));